FROM python:2
WORKDIR  /code
COPY requirements*.txt ./
RUN pip install -r requirements.txt
COPY . .
EXPOSE 5000
CMD python ./app/app.py
