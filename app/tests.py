from app import application
import json
import sys
import unittest


class TestArithmetic(unittest.TestCase):
    application.testing = True
    client = application.test_client()
    url = '/arithmetic'

    def test_basic_sum_should_pass(self):
        x, y, operation = 34, 50, '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 84

    def test_basic_subtraction_should_pass(self):
        x, y, operation = 34, 50, '-'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == -16

    def test_basic_multiplication_should_pass(self):
        x, y, operation = 50, 50, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 2500

    def test_basic_division_should_pass(self):
        x, y, operation = 50, 50, '/'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 1

    def test_basic_negative_multiplications_returns_positive_number_should_pass(self):
        x, y, operation = -50, -50, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 2500

    def test_one_val_negative_multiplication_returns_negative_should_pass(self):
        x, y, operation = 50, -50, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == -2500

        x, y, operation = -50, 50, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == -2500

    def test_negative_division_returns_positive_number_should_pass(self):
        x, y, operation = -50, -50, '/'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 1

    def test_one_val_negative_division_returns_negative_should_pass(self):
        x, y, operation = 50, -50, '/'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == -1

        x, y, operation = -50, 50, '/'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == -1

    # invalid calculation
    def test_divided_by_0_should_fail(self):
        x, y, operation = 50, 0, '/'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and json_response['status'] == 'integer division or modulo by zero'

    def test_multiply_by_0_returns_0_should_pass(self):
        x, y, operation = 50, 0, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 0

    def test_x_and_y_are_numeric_strings_should_pass(self):
        x, y, operation = '1', '2', '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 3

    def test_x_and_y_are_long_should_pass(self):
        x, y, operation = sys.maxsize, sys.maxsize, '*'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'results' in json_response and json_response['results'] == 85070591730234615847396907784232501249 \
               and type(json_response['results']) == long

    # invalid data types
    def test_x_and_y_are_floats_should_fail(self):
        x, y, operation = 2.0, 2.5, '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']

    def test_x_and_y_are_alphachars_should_fail(self):
        x, y, operation = 'a', 'b', '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']

    def test_x_and_y_are_alphanumeric_should_fail(self):
        x, y, operation = 'a', 1, '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']

        x, y, operation = 1, 'a', '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']

    def test_x_and_y_are_special_chars_should_fail(self):
        x, y, operation = '[;][---', '.,/;[]', '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']

    def test_x_and_y_are_none(self):
        x, y, operation = '', '', '+'
        response = self.client.post(self.url, query_string={'x': x, 'y': y, 'operation': operation})
        json_response = json.loads(response.data)
        assert 'status' in json_response and '400 Bad Request' in json_response['status']


if __name__ == "__main__":
    unittest.main()
