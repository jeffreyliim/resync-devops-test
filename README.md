## Resync DevOps Test

### Project setup
- Ensure you have docker installed.

```shell script
$ cd /path/to/resync-devops-test 

$ docker-compose up -d

$ curl 0.0.0.0:5000/users

### run tests
$ docker-compose run workspace python ./app/tests.py
```
